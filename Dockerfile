FROM credsre/python3ds:latest
RUN export DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && \
    apt-get -yq install gcc && \
    apt-get install -y python3-pip awscli && \
    pip3 install --upgrade pip
COPY . /root/enigma_error_daily_reports
WORKDIR /root/enigma_error_daily_reports
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
COPY . ./
RUN chmod +x boot.sh
CMD ["/root/enigma_error_daily_reports/boot.sh"]