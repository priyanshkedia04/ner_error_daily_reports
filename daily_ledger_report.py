from collections import defaultdict, OrderedDict
import pandas as pd
from ner_functools import find_paths, construct_dataframe, get_lattice_pattern, return_df_ner_error_type

def get_entities_name(df):
    credit_ents = set()
    debit_ents = set()
    for i in range(df.shape[0]):
        val = df.ml_attributes.iloc[i]
        ents = val['ner_model']['entities']
        if val['classification']['sms_type'] == 'Credit':
            credit_ents = credit_ents.union(set(ents.keys()))
        elif val['classification']['sms_type'] == 'Debit':
            debit_ents = debit_ents.union(set(ents.keys()))
    return sorted(list(credit_ents)), sorted(list(debit_ents))

def return_debit_entities(debit_ents):
    return OrderedDict(zip(debit_ents, ['missing']*len(debit_ents)))

def return_credit_entities(credit_ents):
    return OrderedDict(zip(credit_ents, ['missing']*len(credit_ents)))

def get_errors_per_entitiy(df):
    credit_error_per_entity_dict = defaultdict(list)
    debit_error_per_entity_dict = defaultdict(list)

    for i in range(df.shape[0]):
        sms = df.body.iloc[i]
        val = df.ml_attributes.iloc[i]
        entities = val['ner_model']['entities']
        classification = val['classification']['sms_type']

        for ent in entities:
            if entities[ent] == 'error':
                if classification == 'Credit':
                    credit_error_per_entity_dict[ent].append(sms)
                elif classification == 'Debit':
                    debit_error_per_entity_dict[ent].append(sms)

    return debit_error_per_entity_dict, credit_error_per_entity_dict

def get_orderedDict_entity(data):
    credit_orderedDict = defaultdict(list)
    debit_orderedDict = defaultdict(list)
    credit_ents = ['account_number', 'balance_amount', 'balance_cur', 'beneficiary', 'card_number', 'credit_amount', 'cur',
                   'description', 'transaction_date', 'transaction_time']
    debit_ents = ['account_number', 'balance_amount', 'balance_cur', 'beneficiary', 'card_number', 'cur', 'debit_amount', 
                  'description', 'limit_amount', 'limit_cur', 'transaction_date', 'transaction_time', 'used_cur', 'used_limit']
    #credit_ents, debit_ents = get_entities_name(data)
    for i in range(data.shape[0]):
        val = data.ml_attributes.iloc[i]
        sms = data.body.iloc[i]
        classification = val['classification']['sms_type']
        if classification == 'Credit':
            ner_error = return_credit_entities(credit_ents)
        elif classification == 'Debit':
            ner_error = return_debit_entities(debit_ents)
        else:
            continue
        flag = 0
        for ent in val['ner_model']['entities']:
            if val['ner_model']['entities'][ent] == 'error':
                flag += 1
                ner_error[ent] = 'error'
            else:
                ner_error[ent] = 'ok'
        if flag > 1:
            if classification == 'Credit':
                credit_orderedDict[tuple(ner_error.items())].append(sms)
            else:
                debit_orderedDict[tuple(ner_error.items())].append(sms)

    return credit_orderedDict, debit_orderedDict


def add_class_and_concat(credit_df, debit_df):
    credit_df['Class'] = ['Credit'] * credit_df.shape[0]
    debit_df['Class'] = ['Debit'] * debit_df.shape[0]
    df = pd.concat([debit_df, credit_df], ignore_index=True)
    df = df.sort_values(by='Count', ascending=False)
    return df

def main(directory, file):
    final_paths = find_paths(directory)
    df = construct_dataframe(final_paths)
    #file_name = 's3://prod-datascience-sms/priyansh/ner_reports/{}_ledger_error_report.xlsx'.format(file)
    file_name = '{}_ledger_error_report.xlsx'.format(file)
    writer = pd.ExcelWriter(file_name, engine='xlsxwriter')
    
    #print('Ledger Report: Error per Entity Report....')
    debit_per_entity_dict, credit_per_entity_dict = get_errors_per_entitiy(df)
    debit_per_entity_df = return_df_ner_error_type(debit_per_entity_dict)
    credit_per_entity_df = return_df_ner_error_type(credit_per_entity_dict)
    per_entity_df = add_class_and_concat(credit_per_entity_df, debit_per_entity_df)
    #print('Running Lattice ....')
    per_entity_df = get_lattice_pattern(per_entity_df)
    per_entity_df.to_excel(writer, sheet_name='ledger_error_per_entity', index = False)
    
    del debit_per_entity_dict, credit_per_entity_dict, debit_per_entity_df, credit_per_entity_df, per_entity_df
    
    #print('Leder Report: OrderDict Error Report...')
    credit_orderedDict, debit_orderedDict = get_orderedDict_entity(df)
    del df
    credit_orderedDict_df = return_df_ner_error_type(credit_orderedDict)
    debit_orderedDict_df = return_df_ner_error_type(debit_orderedDict)
    orderedDict_df = add_class_and_concat(credit_orderedDict_df, debit_orderedDict_df)
    del credit_orderedDict, debit_orderedDict, credit_orderedDict_df, debit_orderedDict_df
    #print('Running Lattice ....')
    orderedDict_df = get_lattice_pattern(orderedDict_df)
    orderedDict_df.to_excel(writer, sheet_name = 'ledger_error_orderedDict', index = False)
    writer.save()


