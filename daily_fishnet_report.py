from collections import defaultdict, OrderedDict
from ner_functools import find_paths, construct_dataframe, get_lattice_pattern, return_df_ner_error_type
import pandas as pd

def get_entities_name(df):
    all_ents = set()
    for i in range(df.shape[0]):
        ents = df.ml_attributes.iloc[i]['ner_model']['entities']
        all_ents = all_ents.union(set(ents.keys()))

    return sorted(list(all_ents))


def return_entities(all_ents):
    return OrderedDict(zip(all_ents, ['missing'] * len(all_ents)))


def get_errors_per_entitiy(df):
    error_per_entity_dict = defaultdict(list)
    for i in range(df.shape[0]):
        sms = df.body.iloc[i]
        entities = df.ml_attributes.iloc[i]['ner_model']['entities']
        for ent in entities:
            if entities[ent] == 'error':
                error_per_entity_dict[ent].append(sms)
    return error_per_entity_dict


def get_orderedDict_entity(data):
    ent_orderedDict = defaultdict(list)
    all_ents = get_entities_name(data)
    for i in range(data.shape[0]):
        val = data.ml_attributes.iloc[i]
        sms = data.body.iloc[i]
        ner_error = return_entities(all_ents)
        flag = 0
        for ent in val['ner_model']['entities']:
            if val['ner_model']['entities'][ent] == 'error':
                flag += 1
                ner_error[ent] = 'error'
            else:
                ner_error[ent] = 'ok'
        if flag > 1:
            ent_orderedDict[tuple(ner_error.items())].append(sms)
    return ent_orderedDict


def main(directory, file):
    final_paths = find_paths(directory)
    df = construct_dataframe(final_paths)
    file_name = '{}__statement_error_report.xlsx'.format(file)
    #file_name = 's3://prod-datascience-sms/priyansh/ner_reports/{}__statement_error_report.xlsx'.format(file)
    writer = pd.ExcelWriter(file_name, engine='xlsxwriter')
    
    #print('Statement Report: Running Error per Entity....')
    per_entity_dict = get_errors_per_entitiy(df)
    per_entity_df = return_df_ner_error_type(per_entity_dict)
    #print('Running Lattice ....')
    per_entity_df = get_lattice_pattern(per_entity_df)
    per_entity_df.to_excel(writer, sheet_name='statement_error_per_entity', index = False)
    del per_entity_dict, per_entity_df
    
    #print('Statement Report: Running OrderDict Error...')
    ent_orderedDict = get_orderedDict_entity(df)
    ent_orderedDict_df = return_df_ner_error_type(ent_orderedDict)
    del df, ent_orderedDict
    #print('Running Lattice ....')
    ent_orderedDict_df = get_lattice_pattern(ent_orderedDict_df)
    ent_orderedDict_df.to_excel(writer, sheet_name='statement_error_orderedDict', index = False)
    writer.save()
