import daily_fishnet_report
import daily_ledger_report
import argparse

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-d", "--date", required=True, help="DD-MM-YYYY Date for the NER Error report")
    args = vars(ap.parse_args())
    file = args["date"]
    day, mon, year = file.split('-')
    directory_ledger = 's3://prod-enigma/ner_error/ledger/{}/{}/{}/'.format(year, mon, day)
    directory_statement = 's3://prod-enigma/ner_error/statement/{}/{}/{}/'.format(year, mon, day)
    daily_ledger_report.main(directory_ledger, '-'.join([day, mon, year]))
    daily_fishnet_report.main(directory_statement, '-'.join([day, mon, year]))