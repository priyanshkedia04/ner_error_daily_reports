import s3fs
from collections import deque
import pandas as pd
import multiprocessing as mp
import numpy as np
import boto3

s3_get_object = boto3.client('s3')
num_part = mp.cpu_count() - 1
fs = s3fs.S3FileSystem()


class LoadData:

    def get_file_from_s3(self, bucket, path):
        obj = s3_get_object.get_object(Bucket=bucket, Key=path)
        return obj['Body']

    def load_data_froms3(self, file_list):
        data_frames = []
        for path in file_list:
            p = path[12:]
            temp = pd.read_json(self.get_file_from_s3("prod-enigma", p), compression='gzip', lines=True)
            temp = temp[['body', 'ml_attributes']]
            data_frames.append(temp)
        return pd.concat(data_frames)

    def parallelize_func(self, files, func):
        n_p = min(len(files), num_part)
        pool = mp.Pool(n_p)
        file_split = np.array_split(files, n_p)
        df = pd.concat(pool.map(func, file_split))
        pool.close()
        pool.join()
        return df


def find_paths(directory):
    final_paths = []
    queue = deque()

    for s3_path in fs.ls(directory):
        queue.append(s3_path)

    while queue:
        current_path = queue.pop()
        if '.gz' in current_path:
            final_paths.append(current_path)
        else:
            for s3_path in fs.ls(current_path):
                queue.append(s3_path)

    return final_paths


def construct_dataframe(final_paths):
    ld = LoadData()
    df = ld.parallelize_func(final_paths, ld.load_data_froms3)
    return df


def return_df_ner_error_type(dict_error):
    arr = list(dict_error.items())
    for i in range(len(arr)):
        arr[i] = list(arr[i])
        arr[i].append(len(arr[i][1]))
    arr = sorted(arr, key=lambda x: x[2], reverse=True)
    df = pd.DataFrame(arr, columns=['NER Error Type', 'SMS', 'Count'])
    return df


def get_lattice_pattern(data):
    from lattice.lattice import Lattice
    print('SUM: ', data['Count'].sum())
    thresh_cnt = int(data['Count'].sum() * 0.001)
    print("Thresh", thresh_cnt)
    data['lattice'] = [None] * data.shape[0]
    for i in range(len(data)):
        sms = data.SMS[i]
        if len(sms) < thresh_cnt:
            data.drop([i], inplace=True)  # if the number of SMS are less that threshold we will remove that row
            continue
        l = Lattice(sms)
        try:
            l_df = l.pandas()
            l_df = l_df.sort_values(by='pattern_importance_score', ascending=False)
            ner_error_lattice = {}
            for j in range(min(100, len(l_df))):
                pattern = l_df.pattern[j]
                descriptions = list(l_df.original_descriptions[j])
                ner_error_lattice[pattern] = {'count': l_df['count'][j], 'cluster': l_df.cluster[j],
                                              'descriptions': descriptions[:2]}
            data.lattice[i] = ner_error_lattice
        except:
            data.lattice[i] = data.SMS[i]  # if lattice shows some error then twe will have the list of SMS
    data.drop(['SMS'], axis=1, inplace=True)
    return data
